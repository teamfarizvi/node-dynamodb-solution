export default {
    UsersTable: {
        Type: 'AWS::DynamoDB::Table',
                DeletionPolicy: 'Retain',
                Properties: {
                    TableName: '${self:provider.environment.USERS_TABLE}',
                    AttributeDefinitions: [
                        { AttributeName: 'id', AttributeType: 'S' }
                    ],
                    KeySchema: [
                        { AttributeName: 'id', KeyType: 'HASH' }
                    ],
                    ProvisionedThroughput: {
                        ReadCapacityUnits: '${self:custom.TABLE_THROUGHPUT}',
                        WriteCapacityUnits: '${self:custom.TABLE_THROUGHPUT}'
                    }
                }
      }
}