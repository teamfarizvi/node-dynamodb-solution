export default {
    createUser: {
        handler: 'handler.createUser',
        events: [
            {
                http: {
                    method: 'POST',
                    path: '/user',
                    cors: true
                }
            }
        ]
    },
    getUsers: {
        handler: 'handler.getUsers',
        events: [
            {
                http: {
                    method: 'POST',
                    path: '/users',
                    cors: true
                }
            }
        ]
    }
}