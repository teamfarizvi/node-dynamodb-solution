import type { Serverless } from 'serverless/aws';

// import hello from '@functions/hello';

// DynamoDB Table
import dynamoDbTables from './resources/dynamodb-tables';

// Functions
import functions from './resources/functions';

const serverlessConfiguration: Serverless = {
  service: 'serverless-user-profile',
  frameworkVersion: '>=1.72.0',
  custom: {
    
    region: '${opt:region, self:provider.region}',
    stage: '${opt:stage, self:provider.stage}',
    users_table: '${self:service}-users-table-${opt:stage, self:provider.stage}',
    table_throughputs: {
      prod: 5,
      default: 1,
    },
    table_throughput: '${self:custom.TABLE_THROUGHPUTS.${self:custom.stage}, self:custom.table_throughputs.default}',
    dynamodb: {
      stages: ['dev'],
      start: {
        port: 8008,
        inMemory: true,
        heapInitial: '200m',
        heapMax: '1g',
        migrate: true,
        seed: true,
        convertEmptyValues: true,
        // Uncomment only if you already have a DynamoDB running locally
        // noStart: true
      }
    },
    ['serverless-offline']: {
      httpPort: 3000,
      babelOptions: {
        presets: ["env"]
      }
    }
  },
  plugins: [
    'serverless-bundle',
    'serverless-dynamodb-local',
    'serverless-offline',
    'serverless-dotenv-plugin',
  ],
  provider: {
    name: 'aws',
    runtime: 'nodejs14.x',
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    lambdaHashingVersion: 20201221,
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      REGION: '${self:custom.region}',
      STAGE: '${self:custom.stage}',
      USERS_TABLE: '${self:custom.users_table}',
    },
    iamRoleStatements: [
      {
        Effect: 'Allow',
        Action: [
            'dynamodb:DescribeTable',
            'dynamodb:Query',
            'dynamodb:Scan',
            'dynamodb:GetItem',
            'dynamodb:PutItem',
            'dynamodb:UpdateItem',
            'dynamodb:DeleteItem'
        ],
        Resource: [
          {"Fn::GetAtt": [ 'UsersTable', 'Arn' ]}
        ]
      }
    ]
  },
  // import the function via paths
  functions: functions,
  resources: {
    Resources: dynamoDbTables
  },
  package: {
    individually: true
  }
};

module.exports = serverlessConfiguration;
