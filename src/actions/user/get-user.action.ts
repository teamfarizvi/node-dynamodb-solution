import {
    APIGatewayProxyHandler,
    APIGatewayEvent,
    Context,
    APIGatewayProxyResult
} from 'aws-lambda';
import 'source-map-support/register';

// Models
import ResponseModel from "../../models/response.model";

// Services
import DatabaseService from "../../services/database.service";

// utils
import { validateAgainstConstraints } from "../../utils/util";

// Define the request constraints
import requestConstraints from '../../constraints/user/get.constraint.json';

export const getUsers: APIGatewayProxyHandler = (event: APIGatewayEvent, _context: Context): Promise<APIGatewayProxyResult> => {
    // Initialize response variable
    let response;

    // Parse request parameters
    const requestData = JSON.parse(event.body);

    // Initialise database service
    const databaseService = new DatabaseService();
    
    // Destructure request data
    const { id } = requestData

    // Destructure process.env
    const { USERS_TABLE } = process.env;

    // Initialise DynamoDB QUERY parameters
    const params = {
        TableName: USERS_TABLE,
        IndexName : 'user_index',
        KeyConditionExpression : '',
        ExpressionAttributeValues : {},
        Key: {key: ''}
    };


    // Validate against constraints
    return databaseService.get(params)
        .then( async (data) => {
        

            // Query table for tasks with the listId
            const results = await databaseService.query(params);
            const users = results?.Items?.map((user) => {
                return {
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    username: user.username,
                    email: user.email,
                }
            });

            // Set Success Response with data
            response = new ResponseModel({
                ...data.Item,
                usersCount: users?.length,
                users: users,
            }, 200, 'Users successfully retrieved');

        })
        .catch((error) => {
            // Set Error Response
            response = (error instanceof ResponseModel) ? error : new ResponseModel({}, 500, 'Users not found');
        })
        .then(() => {
            // Return API Response
            return response.generate()
        });
}