import {
    APIGatewayProxyHandler,
    APIGatewayEvent,
    Context,
    APIGatewayProxyResult,
} from 'aws-lambda';
import { KMS } from 'aws-sdk';
import 'source-map-support/register';

// Models
import UserModel from "../../models/user.model";
import ResponseModel from "../../models/response.model";

// Services
import DatabaseService from "../../services/database.service";

// utils
import { validateAgainstConstraints } from "../../utils/util";

// Define the request constraints
import requestConstraints from '../../constraints/user/create.constraint.json';

const encryptData = async (value: string) => {
    const kms = new KMS({region: process.env.REGION});

    const params = {
        KeyId: `arn:aws:${process.env.region}`,
        Plaintext: value
    };

    return new Promise((resolve, reject) => {
        kms.encrypt(params, (err, data) => {
            if(err) reject(err);
            else resolve(data.CiphertextBlob);
        })
    });

}

export const createUser: APIGatewayProxyHandler = (event: APIGatewayEvent, _context: Context): Promise<APIGatewayProxyResult> => {
    // Initialize response variable
    let response;
    
    // Parse request parameters
    const requestData = JSON.parse(event.body);
    
    // Validate against constraints
    return validateAgainstConstraints(requestData, requestConstraints)
        .then(async () => {
            // Initialise database service
            const databaseService = new DatabaseService();
        
            // Initialise and hydrate model
            const userModel = new UserModel(requestData);
        
            // Get model data
            const data = userModel.getEntityMappings();
        
            const encryptedPassword = await encryptData(data.credentials);


            // Initialise DynamoDB PUT parameters
            const params = {
                TableName: process.env.USERS_TABLE,
                Item: {
                    id: data.id,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    username: data.username,
                    credentials: encryptedPassword,
                    email: data.email,
                    createdAt: data.timestamp,
                    updatedAt: data.timestamp,
                }
            }
            // Inserts item into DynamoDB table
            await databaseService.create(params);
            return data.id;
        })
        .then((id) => {
            // Set Success Response
            response = new ResponseModel({ id }, 200, 'User successfully created');
        })
        .catch((error) => {
            // Set Error Response
            response = (error instanceof ResponseModel) ? error : new ResponseModel({}, 500, 'User cannot be created');
        })
        .then(() => {
            // Return API Response
            return response.generate()
        });
}
