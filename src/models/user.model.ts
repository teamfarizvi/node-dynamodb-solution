import { v4 as UUID } from 'uuid';

// Interfaces
interface IProps {
    id?: string;
    firstName: string;
    lastName: string;
    username: string;
    credentials: string;
    email: string;
}

interface IUserInterface extends IProps {
    timestamp: number;
}

export default class UserModel {
    private _id: string;
    setId(value: string) {
        this._id = value !== '' ? value : null;
    }
    getId() {
        return this._id;
    }

    private _firstName: string;
    setFirstName(value: string) {
        this._firstName = value !== '' ? value : null;
    }
    getFirstName() {
        return this._firstName;
    }

    private _lastName: string;
    setLastName(value: string) {
        this._lastName = value !== '' ? value : null;
    }
    getLastName() {
        return this._lastName;
    }

    private _username: string;
    setUsername(value: string) {
        this._username= value !== '' ? value : null;
    }
    getUsername() {
        return this._username;
    }

    private _credentials: string;
    setCredentials(value: string) {
        this._credentials = value !== '' ? value : null;
    }
    getCredentials() {
        return this._credentials;
    }

    private _email: string;
    setEmail(value: string) {
        this._email = value !== '' ? value : null;
    }
    getEmail() {
        return this._email;
    }

    constructor({   
        id = UUID(),
        firstName,
        lastName,
        username,
        credentials,
        email
    }: IProps) {
        this._id = id;
        this._firstName = firstName;
        this._lastName = lastName;
        this._username = username;
        this._credentials = credentials;
        this._email = email;
    }

    getEntityMappings(): IUserInterface {
        return {
            id: this.getId(),
            firstName: this.getFirstName(),
            lastName: this.getLastName(),
            username: this.getUsername(),
            credentials: this.getCredentials(),
            email: this.getEmail(),
            timestamp: new Date().getTime()
        };
    }
}