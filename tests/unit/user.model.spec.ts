import UserModel from '../../src/models/user.model';
import userMock from '../mocks/user.mock.json';

describe('UserModel tests', () => {
    describe('Entity mapping tests', () => {
        it('should return user data', () => {
            const userModel = new UserModel(userMock);

            expect(userModel.getEntityMappings()).toEqual({
                id: userMock.id,
                firstName: userMock.firstName,
                lastName: userMock.lastName,
                username: userMock.username,
                credentials: userMock.credentials,
                email: userMock.email,
                timestamp: userModel.getEntityMappings().timestamp
            });
        });
    });

    describe('Ensure entity hydration', () => {
        it('should be able to get user the hydrated variables from the model', () => {
            const userModel = new UserModel(userMock);

            expect(userModel.getId()).toEqual(userMock.id);
            expect(userModel.getFirstName()).toEqual(userMock.firstName);
            expect(userModel.getLastName()).toEqual(userMock.lastName);
            expect(userModel.getUsername()).toEqual(userMock.username);
            expect(userModel.getCredentials()).toEqual(userMock.credentials);
            expect(userModel.getEmail()).toEqual(userMock.email);
        });
    })
});