import ResponseModel, { STATUS_MESSAGES } from '../../src/models/response.model';
import responseMock  from '../mocks/response.mock.json';

describe('ResponseModel tests', () => {

    describe('Setting variable tests', () => {
        const responseModel = new ResponseModel();
        
        it('should set the status code correctly', () => {
            responseModel.setCode(responseMock.code);
            expect(responseModel.getCode()).toEqual(responseMock.code);
        });

        it('should set the message correctly', () => {
            responseModel.setMessage(responseMock.message);
            expect(responseModel.getMessage()).toEqual(responseMock.message);
        });
    });

    describe('Test entity mappings', () => {
        const responseModel = new ResponseModel(responseMock.data, responseMock.code, responseMock.message);

        it('should generate a response object', () => {
            expect(responseModel.generate()).toEqual({
                statusCode: responseMock.code,
                headers: responseMock.headers,
                body: JSON.stringify({
                    data: responseMock.data,
                    message: responseMock.message,
                    status: STATUS_MESSAGES[responseMock.code],
                })
            });
        });
    });
});
