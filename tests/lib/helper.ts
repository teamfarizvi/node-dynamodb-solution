process.env.DYNAMODB_LOCAL_ENDPOINT = "http://localhost:8008";
process.env.DYNAMODB_LOCAL_ACCESS_KEY_ID = "test";
process.env.DYNAMODB_LOCAL_SECRET_ACCESS_KEY = "test";
process.env.DYNAMODB_LOCAL_STAGE = "dev";
process.env.BASE_URL = "http://localhost:3000/dev/";
process.env.USERS_TABLE = "serverless-users-table-dev";