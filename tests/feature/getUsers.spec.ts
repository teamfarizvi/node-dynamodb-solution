import handler from '../lib/actions/handler';

type Response = {
    data: any;
    message: string;
    status: string;
};

const requestData = {
    firstName: 'Test First Name',
    lastName: 'Test Last Name',
    username: 'testusername',
    credentials: 'Test123',
    email: 'testuser@test.com'
};

describe('Get users tests', () => {
    let response, statusCode = 200;

    beforeEach((done) => {
        setTimeout(() => {
            handler.createUser({
                requestData
            })
            .then((body) => {
                statusCode = 200;
                response = body;
                done();
            })
        }, 10000);
    });

    it('should expect a 200 status code', () => {
        expect(statusCode).toEqual(200);
    })

    it('should expect success message', () => {
        expect(response.message).toEqual('User successfully created');
    });

    it('should find record created in the database', (done) => {
        
        setTimeout(()=> {
            handler.getUsers({
                id: response.data.id
            })
            .then((result: any) => {
                expect(result.data.id).toEqual(response.data.id);
                expect(result.data.firstName).toEqual(response.data.firstName);
                expect(result.data.lastName).toEqual(response.data.lastName);
                expect(result.data.username).toEqual(response.data.username);
                expect(result.data.email).toEqual(response.data.email);
                done();
            })
        }, 10000);
    })
})