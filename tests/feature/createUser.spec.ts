import handler from '../lib/actions/handler';

type Response = {
    data: any;
    message: string;
    status: string;
}

describe('Create user tests', () => {
    let response, statusCode = 200;

    beforeEach(() => {
        setTimeout(() => {
            handler.createUser({
                firstName: 'Test First Name',
                lastName: 'Test Last Name',
                username: 'testusername',
                credentials: 'Test123',
                email: 'testuser@test.com'
            })
            .then((body) => {
                statusCode = 200;
                response = body;
            })
        }, 10000);
    });

    it('should expect a 200 status code', () => {
        expect(statusCode).toEqual(200);
    })

    it('should expect success message', () => {
        expect(response.message).toEqual('User successfully created');
    })
})